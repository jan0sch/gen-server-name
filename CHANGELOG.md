# ChangeLog for gen-server-name

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## Conventions when editing this file.

Please follow the listed conventions when editing this file:

- one subsection per version
- reverse chronological order (latest entry on top)
- write all dates in iso notation (`YYYY-MM-DD`)
- each version should group changes according to their impact:
    - `Added` for new features.
    - `Changed` for changes in existing functionality.
    - `Deprecated` for once-stable features removed in upcoming releases.
    - `Removed` for deprecated features removed in this release.
    - `Fixed` for any bug fixes.
    - `Security` to invite users to upgrade in case of vulnerabilities.

## Unreleased

## 1.0.9 (2024-08-13)

### Changed

- dependency updates

### Fixed

- name sources for Shakespeare and Star names mixed up

## 1.0.8 (2024-05-15)

### Changed

- dependency updates

## 1.0.7 (2023-05-02)

### Fixed

- big sources dominating random generation

### Removed

- numbers only entries from NEA source

## 1.0.6 (2023-03-20)

### Added

- add reference numbers of near earth asteroids

## 1.0.5 (2022-10-25)

### Added

- add international list of astronomical star names

## 1.0.4 (2020-12-20)

### Added

- add ancient greek names
- add ancient roman names

## 1.0.3 (2020-11-06)

### Fixed

- escaping of parentheses

## 1.0.2 (2020-02-10)

- add character names from plays of William Shakespeare

## 1.0.1 (2020-02-04)

- add names of star wars characters

## 1.0.0 (2020-01-29)

- initial release

