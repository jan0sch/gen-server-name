# Generate a server name.

[Official Crate](https://crates.io/crates/gen-server-name)

This is a small tool which will generate a suggestion for a server name.
You may specify the domain and the name source pool as parameters.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details how to to 
contribute to the project.

## License

This program is distributed under 3-Clause BSD license. See the file 
[LICENSE](LICENSE) for details.

## Installation

To simply install the binary just issue the `cargo install gen-server-name`
command.

### From source code

Either create a binary with `cargo build --release` and copy it to a place
of your liking or use the `cargo install --path .` command which will 
install it into `$HOME/.cargo/bin`.

## Examples

```text
% gen-server-name
grimfast.example.com
% gen-server-name mydomain.com
dan-gander.mydomain.com
% gen-server-name mydomain.com agreek
cleomenes-of-naucratis.mydomain.com
% gen-server-name mydomain.com aroman
petronius-arbiter.mydomain.com
% gen-server-name mydomain.com darkwing
phineas-sharp.mydomain.com
% gen-server-name mydomain.com islands
iwashijima.mydomain.com
% gen-server-name mydomain.com lotr
finrod.mydomain.com
% gen-server-name mydomain.com starwars
mon-mothma.mydomain.com
% gen-server-name mydomain.com wot
narenwin-barda.mydomain.com
```

The program has basic help compiled in.

```text
% gen-server-name -h
Generate Server Name 1.0.4
Generate a server name suggestion based on source pool and given domain.

USAGE:
    gen-server-name [ARGS]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

ARGS:
    <domain>    The domain to append to the server name.
    <source>    The source for the name suggestions. Either all, agreek, aroman, darkwing, islands, lotr,
                shakespeare, sw or wot. Alternative specification is 'ancient greek', 'ancient roman', 'darkwing
                duck', 'islands of japan', 'lord of the rings', 'william shakespeare', 'starwars', 'star wars' or
                'wheel of time'.
```

