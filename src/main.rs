// ============================================================================
// Copyright 2020 Jens Grassel
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice,
// this list of conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its
// contributors may be used to endorse or promote products derived from this
// software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// ============================================================================
extern crate clap;
extern crate csv;
extern crate deunicode;
extern crate rand;

use clap::{Arg, App};
use deunicode::deunicode;
use rand::prelude::*;
use regex::Regex;
use std::error::Error;
use std::process;

fn run(source: String) -> Result<String, Box<dyn Error>> {
    let mut rdr = csv::Reader::from_reader(source.as_bytes());
    let mut names: Vec<String> = Vec::new();
    for result in rdr.records() {
        let record = result?;
        names.push(record[0].to_string())
    }
    let count = names.len();
    let mut rng = rand::thread_rng();
    let e: usize = rng.gen_range(0..count);
    let replace_hyphens = Regex::new(r"[-]+").unwrap();
    let name = &names[e]
        .to_lowercase()
        .replace(" ", "-")
        .replace("'", "-")
        .replace("/", "")
        .replace("\"", "")
        .replace("(", "")
        .replace(")", "")
        .replace("[", "")
        .replace("]", "")
        .replace(".", "");
    let cleaned_name = replace_hyphens.replace_all(&name, "-");
    // TODO Some unicode characters might be denormalised into a line break (\n)!
    let useable_name = deunicode(&cleaned_name)
        .replace("[?]", "");
    Ok(useable_name.to_string())
}

fn main() {
    let ancient_greek = include_bytes!("ancient-greek.txt");
    let ancient_greek_str = String::from_utf8_lossy(ancient_greek).to_string();
    let ancient_roman = include_bytes!("ancient-roman.txt");
    let ancient_roman_str = String::from_utf8_lossy(ancient_roman).to_string();
    let darkwing = include_bytes!("darkwing-duck.txt");
    let darkwing_str = String::from_utf8_lossy(darkwing).to_string();
    let islands_japan = include_bytes!("islands-japan.txt");
    let islands_japan_str = String::from_utf8_lossy(islands_japan).to_string();
    let lotr = include_bytes!("lotr.txt");
    let lotr_str = String::from_utf8_lossy(lotr).to_string();
    let nea = include_bytes!("nea.txt");
    let nea_str = String::from_utf8_lossy(nea).to_string();
    let shakespeare = include_bytes!("shakespeare.txt");
    let shakespeare_str = String::from_utf8_lossy(shakespeare).to_string();
    let stars = include_bytes!("stars.txt");
    let stars_str = String::from_utf8_lossy(stars).to_string();
    let starwars = include_bytes!("star-wars.txt");
    let starwars_str = String::from_utf8_lossy(starwars).to_string();
    let wot = include_bytes!("wheel-of-time.txt");
    let wot_str = String::from_utf8_lossy(wot).to_string();

    let sources = [
        ancient_greek_str,
        ancient_roman_str,
        darkwing_str,
        islands_japan_str,
        lotr_str,
        nea_str,
        shakespeare_str,
        stars_str,
        starwars_str,
        wot_str
    ];
    let mut rng = rand::thread_rng();
    let random_source: usize = rng.gen_range(0..sources.len());
    
    let params = App::new("Generate Server Name")
        .version(env!("CARGO_PKG_VERSION"))
        .about("Generate a server name suggestion based on source pool and given domain.")
        .arg(Arg::with_name("domain")
            .help("The domain to append to the server name.")
        )
        .arg(Arg::with_name("source")
            .help("The source for the name suggestions. Either all, agreek, aroman, darkwing, islands, lotr, nea, shakespeare, stars, sw or wot. Alternative specification is 'ancient greek', 'ancient roman', 'darkwing duck', 'islands of japan', 'lord of the rings', 'near earth asteroids', 'william shakespeare', 'star names', 'starwars', 'star wars' or 'wheel of time'.")
        )
        .get_matches();

    // Get the domain name or provide a default i.e. `example.com`.
    let domain = params.value_of("domain").unwrap_or("example.com");
    // Get the source pool for name suggestions.
    let source = match params.value_of("source").unwrap_or("all").to_lowercase().as_str() {
        "agreek"      | "ancient greek"                     => &sources[0],
        "aroman"      | "ancient roman"                     => &sources[1],
        "darkwing"    | "darkwing duck"                     => &sources[2],
        "islands"     | "islands of japan"                  => &sources[3],
        "lotr"        | "lord of the rings"                 => &sources[4],
        "nea"         | "near earth asteroids"              => &sources[5],
        "shakespeare" | "william shakespeare"               => &sources[6],
        "stars"       | "star names"                        => &sources[7],
        "sw"          | "starwars"            | "star wars" => &sources[8],
        "wot"         | "wheel of time"                     => &sources[9],
        _                                                   => &sources[random_source],
    };
    match run(source.to_string()) {
        Err(e) => {
            println!("{}", e);
            process::exit(1);
        },
        Ok(name) => {
            let mut server = name;
            server.push_str(&".");
            server.push_str(&domain);
            println!("{}", server);
        },
    }
}
